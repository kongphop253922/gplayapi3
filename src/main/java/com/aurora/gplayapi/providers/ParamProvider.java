package com.aurora.gplayapi.providers;

import com.aurora.gplayapi.modals.ApiBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ParamProvider {
    public static Map<String, String> getDefaultLoginParams(ApiBuilder builder) {
        final Map<String, String> params = new HashMap<String, String>();
        if (StringUtils.isNotEmpty(builder.getGsfId()))
            params.put("androidId", builder.getGsfId());
        params.put("sdk_version", String.valueOf(builder.getDeviceInfoProvider().getSdkVersion()));
        params.put("Email", builder.getEmail());
        params.put("google_play_services_version", String.valueOf(builder.getDeviceInfoProvider().getPlayServicesVersion()));
        params.put("device_country", builder.getLocale().getCountry().toLowerCase());
        params.put("lang", builder.getLocale().getLanguage().toLowerCase());
        params.put("callerSig", "38918a453d07199354f8b19af05ec6562ced5788");
        return params;
    }

    public static Map<String, String> getTokenParams(String aasToken) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("service", "oauth2:https://www.googleapis.com/auth/googleplay");
        params.put("app", "com.android.vending");
        params.put("oauth2_foreground", "1");
        params.put("token_request_options", "CAA4AVAB");
        params.put("check_email", "1");
        params.put("Token", aasToken);
        params.put("client_sig", "38918a453d07199354f8b19af05ec6562ced5788");
        params.put("callerPkg", "com.google.android.gms");
        params.put("system_partition", "1");
        params.put("_opt_is_called_from_account_manager", "1");
        params.put("is_called_from_account_manager", "1");
        return params;
    }
}
